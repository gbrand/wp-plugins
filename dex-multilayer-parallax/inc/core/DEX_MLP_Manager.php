<?php

defined( 'ABSPATH' ) or die();

/***************************************
* DEX MULTILAYER PARALLAX MANAGER CLASS
***************************************/
class DEX_MLP_Manager {

	private static $inst = null, $mlp_inst = 0;

	private function __construct(){
		$this->register();
		$this->init_plugin();
	}

	static function run(){
		if( !self::$inst ){
			self::$inst = new DEX_MLP_Manager;
		}
		return self::$inst;
	}

	private function register(){
		register_post_type( 'dex-mlp-parallax', array(
			'public'              => false,
			'show_in_nav_menus'   => false,
			'show_ui'             => false,
			'exclude_from_search' => true,
			'has_archive'         => false,
			'rewrite'             => false,
		) );
		add_shortcode( 'dex_mlp_parallax', array( $this, 'do_shortcode' ) );
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );
	}

	private function init_plugin(){
		if( is_admin() ){
			require_once DEX_MLP_CORE . 'DEX_MLP_Backend.php';
			$plugin = new DEX_MLP_Backend();
		} else {
			require_once DEX_MLP_CORE . 'DEX_MLP_Frontend.php';
			$plugin = new DEX_MLP_Frontend();
		}
		$addons = new DEX_MLP_Addons();
	}

	public function do_shortcode( $atts, $content ){
		$atts = extract( shortcode_atts( array(
			'id' => 0,
		), $atts, 'dex_mlp_parallax' ) );
		$id   = absint( $id );
		$mlp  = $this->get_dex_mlp( $id );
		if( $mlp ){
			$mlp     = new DEX_MLP_Instance( $mlp );
			$layers  = array();
			$i       = 2;
			$stellar = $mlp->parallax == 'scroll' ? true : false;
			self::$mlp_inst++;
			foreach( $mlp->layers as $layer ){
				$styles = array(
					'z-index:' . intval( $i ),
					'left:' . $layer->x . 'px',
					'top:' . $layer->y . 'px',
					'opacity:' . $layer->opacity,
					'transform:rotate(' . $layer->rotation . 'rad)',
				);
				if( $layer->width ){
					$styles[] = 'width:' . $layer->width . 'px';
				}
				if( $layer->height ){
					$styles[] = 'height:' . $layer->height . 'px';
				}
				$layers[] = sprintf(
					$stellar ?
						'<div class="dex-mlp-layer" style="%1$s" data-stellar-ratio="%2$f">%3$s</div>':
						'<div class="dex-mlp-layer" style="%1$s" data-move-ratio="%2$f">%3$s</div>',
						esc_attr( implode( ';', $styles ) ), $layer->speed, $layer->html
				);
				$i++;
			}
			$styles = $margins = $classes = array();
			if( $mlp->layout == 'custom' ){
				if( $mlp->width ){
					$styles[] = 'max-width:' . $mlp->width . 'px';
				}
				if( $mlp->height ){
					$styles[] = 'height:' . $mlp->height . 'px';
				}
			}
			if( $mlp->padding_top ){
				$styles[] = 'padding-top:' . $mlp->padding_top . 'px';
			}
			if( $mlp->padding_bottom ){
				$styles[] = 'padding-bottom:' . $mlp->padding_bottom . 'px';
			}
			$video = '';
			switch( $mlp->background ){
				case 'bg_image':
					$image    = wp_get_attachment_image_src( $mlp->bg_image, 'full' );
					$styles[] = 'background-image:url(\'' . $image[0] . '\')';
					break;
				case 'bg_color':
					$styles[] = 'background-color:' . $mlp->bg_color;
					break;
				case 'bg_video':
					$styles[] = 'background-color:#000';
					$video    = '<div class="dex-mlp-video-bg" data-property="' . esc_attr( json_encode( array(
							'videoURL'     => $mlp->bg_video,
							'containment'  => '#dex-mlp-parallax-' . esc_attr( self::$mlp_inst ),
							'autoPlay'     => true,
							'mute'         => true,
							'showControls' => false,
							'startAt'      => 0,
							'opacity'      => 1,
							'loop'         => true,
							'showYTLogo'   => false,
							'gaTrack'      => false,
						) ) ) . '"></div>';
			}
			$classes[] = 'dex-' . $mlp->parallax;
			$classes[] = 'dex-' . $mlp->layout;
			if( $mlp->force_fw ){
				$classes[] = 'dex-force-fw';
			}
			$zIndex  = count( $mlp->layers ) + 2;
			$overlay = '';
			if( $mlp->overlay ){
				$image   = wp_get_attachment_image_src( $mlp->overlay_img, 'full' );
				$overlay = '<div class="dex-mlp-overlay" style="background-image:url(\'' . esc_attr( $image[0] ) . '\')"></div>';
			}
			if( $mlp->margin_top ){
				$margins[] = 'margin-top:' . $mlp->margin_top . 'px';
			}
			if( $mlp->margin_bottom ){
				$margins[] = 'margin-bottom:' . $mlp->margin_bottom . 'px';
			}
			return '<div class="dex-mlp-wrap" style="' . esc_attr( implode( ';', $margins ) ) . '">
				<div id="dex-mlp-parallax-' . esc_attr( self::$mlp_inst ) . '" class="dex-mlp-parallax ' . esc_attr( implode( ' ', $classes ) ) . '"
					style="' . esc_attr( implode( ';', $styles ) ) . '"
					data-stellar-background-ratio="' . esc_attr( $mlp->bg_speed ) . '">' .
						$video . $overlay .
					'<div class="dex-layer-container">' .
						implode( '', $layers ) .
					'</div>
					<div class="dex-container">
						<div class="dex-mlp-content" style="z-index:' . esc_attr( absint( $zIndex ) ) . '">' . do_shortcode( $content ) . '</div>
					</div>
				</div>
			</div>';
		}
		return do_shortcode( $content );
	}

	public function load_textdomain(){
		load_plugin_textdomain( DEX_MLP_TEXTDOMAIN, false, DEX_MLP_BASENAME . '/languages' );
	}

	private function get_dex_mlp( $id ){
		$post = get_post( $id );
		if( $post && isset( $post->post_content ) ){
			$d = @json_decode( $post->post_content, true );
			if( $d ){
				return $d;
			}
		}
		return;
	}

}
