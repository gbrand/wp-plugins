<?php

defined( 'ABSPATH' ) or die();

/***************************************
* DEX MULTILAYER PARALLAX INSTANCE CLASS
***************************************/
class DEX_MLP_Instance {

	protected $data;

	public function __construct( $a ){
		$this->data['id'] = isset( $a['id'] )
			? absint( $a['id'] ) : 0;
		$this->data['name'] = isset( $a['name'] )
			? $a['name'] : '';
		$this->data['parallax'] = isset( $a['parallax'] ) && $a['parallax'] === 'mouse'
			? 'mouse' : 'scroll';
		$this->data['layout'] = isset( $a['layout'] ) && $a['layout'] === 'fullscreen'
			? 'fullscreen' : 'custom';
		$this->data['width'] = isset( $a['width'] ) && $a['width'] > 0 && $this->data['layout'] == 'custom'
			? absint( $a['width'] ) : 1920;
		$this->data['height'] = isset( $a['height'] ) && $a['height'] > 0 && $this->data['layout'] == 'custom'
			? absint( $a['height'] ) : 1080;
		$this->data['force_fw'] = isset( $a['force_fw'] )
			? (bool) $a['force_fw'] : false;
		$this->data['margin_top'] = isset( $a['margin_top'] )
			? intval( $a['margin_top'] ) : 0;
		$this->data['margin_bottom'] = isset( $a['margin_bottom'] )
			? intval( $a['margin_bottom'] ) : 0;
		$this->data['padding_top'] = isset( $a['padding_top'] )
			? absint( $a['padding_top'] ) : 0;
		$this->data['padding_bottom'] = isset( $a['padding_bottom'] )
			? absint( $a['padding_bottom'] ) : 0;
		$a['background'] = isset( $a['background'] ) ? $a['background'] : '';
		switch( $a['background'] ){
			case 'bg_transparent':
				$b = 'bg_transparent'; break;
			case 'bg_image':
				$b = 'bg_image'; break;
			case 'bg_color':
				$b = 'bg_color'; break;
			case 'bg_video':
				$b = 'bg_video'; break;
			default:
				$b = 'bg_transparent';
		}
		$this->data['background'] = $b;
		$this->data['bg_image'] = isset( $a['bg_image'] )
			? absint( $a['bg_image'] ) : 0;
		$this->data['bg_color'] = isset( $a['bg_color'] )
			? $this->validate_hex_color( $a['bg_color'] ) : '#ffffff';
		$this->data['bg_video'] = isset( $a['bg_video'] )
			? $this->validate_youtube_ID( $a['bg_video'] ) : '';
		$this->data['bg_speed'] = isset( $a['bg_speed'] )
			? $this->speed_in_range( $a['bg_speed'] ) : 0.1;
		$this->data['overlay'] = isset( $a['overlay'] )
			? (bool)$a['overlay'] : false;
		$this->data['overlay_img'] = isset( $a['overlay_img'] )
			? absint( $a['overlay_img'] ) : 0;
		$this->data['layers'] = isset( $a['layers'] ) && is_array( $a['layers'] )
			? $this->get_layer_objects( $a['layers'] ) : array();
	}

	private function validate_hex_color( $HEX ){
		if( preg_match( '/^#[a-f0-9]{3}([a-f0-9]{3})?$/i', $HEX ) ){
			return $HEX;
		} elseif( preg_match( '/^[a-f0-9]{3}([a-f0-9]{3})?$/i', $HEX ) ){
			return '#' . $HEX;
		}
		return '#ffffff';
	}

	private function validate_youtube_ID( $ID ){
		if( preg_match( '/^[A-z0-9_-]{11}$/i', $ID ) ){
			return $ID;
		}
		return '';
	}

	private function speed_in_range( $speed ){
		return $speed >= 0 && $speed <= 1 ? $speed : 0.1;
	}

	private function get_layer_objects( $layers ){
		$objs = array();
		for( $i = 0, $c = count( $layers ); $i < $c; $i++ ){
			$objs[] = new DEX_MLP_Layer( $layers[ $i ] );
		}
		return $objs;
	}

	public function __get( $k ){
		if( isset( $this->data[ $k ] ) ){
			return $this->data[ $k ];
		}
		return;
	}

	public function __toString(){
		return json_encode( $this->data );
	}

}

/***************************************
* DEX MULTILAYER PARALLAX LAYER CLASS
***************************************/
class DEX_MLP_Layer {

	public $html, $x, $y, $width, $height, $rotation, $speed, $opacity;

	public function __construct( $a ){
		$this->html = isset( $a['html'] )
			? $a['html'] : '';
		$this->type = isset( $a['type'] )
			? $this->valid_layer_type( $a['type'] ) : 'text';
		$this->x = isset( $a['x'] )
			? intval( $a['x'] ) : 0;
		$this->y = isset( $a['y'] )
			? intval( $a['y'] ) : 0;
		$this->width = isset( $a['width'] )
			? absint( $a['width'] ) : 50;
		$this->height = isset( $a['height'] )
			? absint( $a['height'] ) : 50;
		$this->rotation = isset( $a['rotation'] )
			? sprintf( '%f', $a['rotation'] ) : 0;
		$this->speed = isset( $a['speed'] )
			? $this->speed_in_range( $a['speed'] ) : 1.25;
		$this->opacity = isset( $a['opacity'] )
			? $this->opacity_in_range( $a['opacity'] ) : 1;
	}

	private function valid_layer_type( $type = 'text' ){
		switch( $type ){
			case 'text': return 'text';
			case 'img' : return 'img';
			default    : return 'text';
		}
	}

	private function speed_in_range( $speed ){
		return $speed >= 0.5 && $speed <= 2 ? sprintf( '%.2f', $speed ) : 1.25;
	}

	private function opacity_in_range( $opacity ){
		return $opacity >= 0 && $opacity <= 1 ? sprintf( '%.1f', $opacity ) : 1;
	}

}
