<?php

defined( 'ABSPATH' ) or die();

/***************************************
* DEX MULTILAYER PARALLAX FRONTEND CLASS
***************************************/
class DEX_MLP_Frontend {

	public function __construct(){
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts' ) );
	}

	public function register_scripts(){
		// load styles
		wp_enqueue_style( 'mb-ytplayer', DEX_MLP_ASSETS . 'libs/mb-ytplayer/css/jquery.mb.YTPlayer.min.css', false, DEX_MLP_VERSION );
		wp_enqueue_style( 'dex-mlp-parallax', DEX_MLP_ASSETS . 'css/dex-mlp-front.css', false, DEX_MLP_VERSION );
		// load scripts
		wp_enqueue_script( 'stellar-js', DEX_MLP_ASSETS . 'libs/stellar-js/jquery.stellar.min.js', array( 'jquery' ), DEX_MLP_VERSION );
		wp_enqueue_script( 'mb-ytplayer', DEX_MLP_ASSETS . 'libs/mb-ytplayer/jquery.mb.YTPlayer.min.js', array( 'jquery' ), DEX_MLP_VERSION );
		wp_enqueue_script( 'dex-mlp-parallax', DEX_MLP_ASSETS . 'js/dex-mlp-front.js', array( 'jquery' ), DEX_MLP_VERSION );
	}

}
