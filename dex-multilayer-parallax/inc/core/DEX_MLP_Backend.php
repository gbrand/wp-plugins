<?php

defined( 'ABSPATH' ) or die();

/***************************************
* DEX MULTILAYER PARALLAX WP-ADMIN BACKEND
***************************************/
class DEX_MLP_Backend {

	private $top_menu = null, $tpl = array();

	public function __construct(){
		add_action( 'admin_menu', array( $this, 'add_menu_page' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_action( 'wp_ajax_dex_mlp_delete', array( $this, 'ajax_delete_parallax' ) );
		add_action( 'wp_ajax_dex_mlp_create', array( $this, 'ajax_create_parallax' ) );
		add_action( 'wp_ajax_dex_mlp_update_parallax', array( $this, 'ajax_update_parallax' ) );
		add_action( 'wp_ajax_dex_mlp_load_layers', array( $this, 'ajax_load_layers' ) );
		add_action( 'wp_ajax_dex_mlp_update_layers', array( $this, 'ajax_update_layers' ) );
	}

	public function register_scripts( $hook ){
		if( $hook === $this->top_menu ){
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'jquery-ui-draggable' );
			wp_enqueue_script( 'jquery-ui-resizable' );
			wp_enqueue_script( 'jquery-ui-sortable' );
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'jquery-ui-rotatable', DEX_MLP_ASSETS . 'libs/jquery-ui-rotatable/jquery.ui.rotatable.min.js',
				array( 'jquery' ), DEX_MLP_VERSION, true
			);
			wp_enqueue_script(
				'dex-mlp-parallax', DEX_MLP_ASSETS . 'js/dex-mlp.js',
				array( 'jquery', 'jquery-ui-draggable', 'jquery-ui-resizable', 'jquery-ui-rotatable', 'wp-color-picker' ),
				DEX_MLP_VERSION, true
			);
			wp_enqueue_style( 'dex-mlp-parallax-icons', DEX_MLP_ASSETS . 'fonts/dex-mlp-icons/dex-mlp-icons.css', false, DEX_MLP_VERSION );
			wp_enqueue_style( 'dex-mlp-parallax', DEX_MLP_ASSETS . 'css/dex-mlp.css', false, DEX_MLP_VERSION );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_media();
			wp_localize_script( 'dex-mlp-parallax', 'DEX_MLP', array(
				'NONCE'       => wp_create_nonce( __CLASS__ ),
				'WP_AJAX'     => admin_url( 'admin-ajax.php' ),
				'i18n'        => array(
					'custom'             => __( 'Custom: ', DEX_MLP_TEXTDOMAIN ),
					'fullscreen'         => __( 'Fullscreen', DEX_MLP_TEXTDOMAIN ),
					'scroll_parallax'    => __( 'Scroll Parallax', DEX_MLP_TEXTDOMAIN ),
					'mouse_parallax'     => __( 'Mouse Parallax', DEX_MLP_TEXTDOMAIN ),
					'force_fullwidth'    => __( 'Force Fullwidth', DEX_MLP_TEXTDOMAIN ),
					'margin_top'         => __( 'Margin Top: ', DEX_MLP_TEXTDOMAIN ),
					'margin_bottom'      => __( 'Margin Bottom: ', DEX_MLP_TEXTDOMAIN ),
					'padding_top'        => __( 'Padding Top: ', DEX_MLP_TEXTDOMAIN ),
					'padding_bottom'     => __( 'Padding Bottom: ', DEX_MLP_TEXTDOMAIN ),
					'confirm_mlp_del'    => __( 'Do you want to delete this parallax?', DEX_MLP_TEXTDOMAIN ),
					'unable_load_layers' => __( 'Unable to load layers!', DEX_MLP_TEXTDOMAIN ),
					'layer'              => __( 'Layer ', DEX_MLP_TEXTDOMAIN ),
					'hide'               => __( 'hide this layer', DEX_MLP_TEXTDOMAIN ),
					'show'               => __( 'show this layer', DEX_MLP_TEXTDOMAIN ),
					'lock'               => __( 'lock this layer', DEX_MLP_TEXTDOMAIN ),
					'unlock'             => __( 'unlock this layer', DEX_MLP_TEXTDOMAIN ),
					'speed_'             => __( 'Speed: ', DEX_MLP_TEXTDOMAIN ),
					'opacity_'           => __( 'Opacity: ', DEX_MLP_TEXTDOMAIN ),
					'speed_ratio'        => __( 'Speed - Ratio', DEX_MLP_TEXTDOMAIN ),
					'opacity'            => __( 'Opacity', DEX_MLP_TEXTDOMAIN ),
					'layer_pos'          => __( 'Layer Position', DEX_MLP_TEXTDOMAIN ),
					'delete_layer'       => __( 'Delete Layer', DEX_MLP_TEXTDOMAIN ),
					'confirm_layers_del' => __( 'Do you want to delete all of the layers?', DEX_MLP_TEXTDOMAIN ),
					'confirm_layer_del'  => __( 'Do you want to delete this layer?', DEX_MLP_TEXTDOMAIN ),
					'text_layer'         => __( 'Text Layer ', DEX_MLP_TEXTDOMAIN ),
					'ARE_YOU_SURE'       => __( 'You have unsaved data in your parallax. Are you sure you want to edit this one and loose any unsaved data?', DEX_MLP_TEXTDOMAIN ),
				),
				'YOUTUBE_IMG' => DEX_MLP_ASSETS . 'img/youtube-logo.jpg',
			) );
		}
	}

	public function add_menu_page(){
		$this->top_menu = add_menu_page(
			__( 'DEX Multilayer Parallax', DEX_MLP_TEXTDOMAIN ),
			__( 'DEX::Multilayer', DEX_MLP_TEXTDOMAIN ),
			'edit_posts',
			'dex-mlp-parallax-manager',
			array( $this, 'render' ),
			DEX_MLP_ASSETS . 'img/dex-icon-20x20.png',
			127
		);
	}

	private function get_dex_mlps(){
		$mlps = get_posts( array(
			'post_status'    => 'publish',
			'post_type'      => 'dex-mlp-parallax',
			'posts_per_page' => -1,
			'orderby'        => 'id',
			'order'          => 'ASC',
		) );
		$b = array(
			'count' => 0,
			'items' => array(),
		);
		if( $mlps ){
			foreach( $mlps as $mlp ){
				$d = @json_decode( $mlp->post_content, true );
				if( $d ){
					$b['items'][] = new DEX_MLP_Instance( array_merge( $d, array(
						'id'   => $mlp->ID,
						'name' => $mlp->post_title,
					) ) );
					$b['count']++;
				}
			}
		}
		return $b;
	}

	private function get_template( $k, $option = null ){
		if( !isset( $tpl[ $k ] ) ){
			$tpl[ $k ] = @file_get_contents( DEX_MLP_TPL . $k . '.tpl' );
		}
		if( $option === 'no-white-space' ){
			return str_replace( array( "\n", "\r", "\t", '  ' ), ' ', $tpl[ $k ] );
		}
		return $tpl[ $k ];
	}

	public function render(){
		$mlps = $this->get_dex_mlps();
		?>
		<div id="dex-mlp-page-wrapper">
			<div id="dex-mlp-header">
				<img src="<?php echo esc_attr( DEX_MLP_ASSETS . 'img/logo.png' ); ?>" alt="logo"/>
	        </div>
			<div id="dex-mlp-create">
				<h2 class="dex-mlp-section-title"><?php _e( 'Create', DEX_MLP_TEXTDOMAIN ); ?></h2>
				<div class="dex-mlp-row">
					<div class="dex-mlp-col12">
						<div id="dex-mlp-main-list"<?php echo !$mlps['count'] ? ' style="display:none"' : ''; ?>>
							<h3>You already created.</h3>
							<ol class="dex-mlp-parallax-list">
								<?php for( $i = 0; $i < $mlps['count']; $i++ ): ?>
								<?php
									$mlp  = $mlps['items'][ $i ];
									$json = (string)$mlp;
									printf(
										$this->get_template( 'parallax-instance' ),
										( $i + 1 ), esc_attr( $json ), esc_html( $mlp->name ),
										__( 'Edit Parallax', DEX_MLP_TEXTDOMAIN ),
										__( 'Edit', DEX_MLP_TEXTDOMAIN ),
										__( 'Delete Parallax', DEX_MLP_TEXTDOMAIN ),
										( $mlp->parallax == 'scroll' ? ' checked="checked"' : '' ),
										( $mlp->parallax == 'mouse' ? ' checked="checked"' : '' ),
										( $mlp->layout == 'fullscreen' ? ' checked="checked"' : '' ),
										( $mlp->layout == 'custom' ? ' checked="checked"' : '' ),
										$mlp->width, $mlp->height,
										( $mlp->force_fw ? ' checked="checked"' : '' ),
										$mlp->margin_top, $mlp->margin_bottom,
										$mlp->padding_top, $mlp->padding_bottom,
										esc_attr( DEX_MLP_ASSETS . 'img/loader.gif' ), $mlp->id,
										__( 'Parallax Name', DEX_MLP_TEXTDOMAIN ),
										__( 'Scroll Parallax', DEX_MLP_TEXTDOMAIN ),
										__( 'Mouse Parallax', DEX_MLP_TEXTDOMAIN ),
										__( 'Fullscreen', DEX_MLP_TEXTDOMAIN ),
										__( 'Custom', DEX_MLP_TEXTDOMAIN ),
										__( 'Force Fullwidth', DEX_MLP_TEXTDOMAIN ),
										__( 'Margin Top', DEX_MLP_TEXTDOMAIN ),
										__( 'Margin Bottom', DEX_MLP_TEXTDOMAIN ),
										__( 'Padding Top', DEX_MLP_TEXTDOMAIN ),
										__( 'Padding Bottom', DEX_MLP_TEXTDOMAIN ),
										__( 'Save Settings', DEX_MLP_TEXTDOMAIN ),
										__( 'Edit Layers', DEX_MLP_TEXTDOMAIN ),
										__( 'Use shortcode', DEX_MLP_TEXTDOMAIN ),
										__( 'or', DEX_MLP_TEXTDOMAIN )
									);
								?>
								<?php endfor; ?>
							</ol>
						</div>
						<form id="dex-mlp-form-0" onsubmit="return false;">
							<h3><?php _e( 'Create New Multi-Layer Parallax.', DEX_MLP_TEXTDOMAIN ); ?></h3>
							<div class="dex-mlp-row">
								<div class="dex-mlp-col4">
									<input type="text" name="name[0]" class="dex-mlp-parallax-name" placeholder="<?php _e( 'Parallax Name', DEX_MLP_TEXTDOMAIN ); ?>"/>
								</div>
								<div class="dex-mlp-col1"></div>
								<div class="dex-mlp-col7">
									<div class="dex-mlp-parallax-type">
										<label>
											<input type="radio" name="parallax[0]" value="scroll" checked="checked" />
											<?php _e( 'Scroll Parallax', DEX_MLP_TEXTDOMAIN ); ?>
										</label>
										<label>
											<input type="radio" name="parallax[0]" value="mouse" />
											<?php _e( 'Mouse Parallax', DEX_MLP_TEXTDOMAIN ); ?>
										</label>
									</div>
									<div class="dex-mlp-parallax-size">
										<label>
											<input type="radio" name="layout[0]" value="fullscreen" checked="checked" />
											<?php _e( 'Fullscreen', DEX_MLP_TEXTDOMAIN ); ?>
										</label>
										<label>
											<input type="radio" name="layout[0]" value="custom" />
											<?php _e( 'Custom', DEX_MLP_TEXTDOMAIN ); ?>
										</label>
										<input type="text" name="width[0]" placeholder="960" />
										<label>x</label>
										<input type="text" name="height[0]" placeholder="480" />
										<label>px</label>
										<label>
											<input type="checkbox" name="force_fw[0]" value="force_fw" />
											<?php _e( 'Force Fullwidth', DEX_MLP_TEXTDOMAIN ); ?>
										</label>

									</div>

								</div>
							</div>
							<div class="dex-mlp-parallax-position">
								<div class="dex-mlp-row">
									<div class="dex-mlp-col3">
										<label><?php _e( 'Margin Top', DEX_MLP_TEXTDOMAIN ); ?></label>
										<input type="text" name="margin_top[0]" placeholder="0" />
										<label>px</label>
									</div>
									<div class="dex-mlp-col3">
										<label><?php _e( 'Margin Bottom', DEX_MLP_TEXTDOMAIN ); ?></label>
										<input type="text" name="margin_bottom[0]" placeholder="0" />
										<label>px</label>
									</div>
									<div class="dex-mlp-col3">
										<label><?php _e( 'Padding Top', DEX_MLP_TEXTDOMAIN ); ?></label>
										<input type="text" name="padding_top[0]" placeholder="0" />
										<label>px</label>
									</div>
									<div class="dex-mlp-col3">
										<label><?php _e( 'Padding Bottom', DEX_MLP_TEXTDOMAIN ); ?></label>
										<input type="text" name="padding_bottom[0]" placeholder="0" />
										<label>px</label>
									</div>
								</div>
							</div>
							<p class="text-center">
								<button type="button" class="btn dex-mlp-save-parallax"><?php _e( 'Save Parallax', DEX_MLP_TEXTDOMAIN ); ?></button>
								<img src="<?php echo esc_attr( DEX_MLP_ASSETS . 'img/loader.gif' ); ?>" alt="" style="display:none;" />
							<p>
							<p class="dex-mlp-response text-center"></p>
						</form>
					</div>
				</div>
			</div>
			<canvas id="dex-mlp-text-checker"></canvas>
			<div id="dex-mlp-editor"></div>
		</div>
		<?php
	}

	public function ajax_delete_parallax(){
		$data    = array(
			'done' => false,
			'msg'  => __( 'Unknown error', DEX_MLP_TEXTDOMAIN ),
		);
		if( isset( $_POST['dex_mlp_nonce'] ) && wp_verify_nonce( $_POST['dex_mlp_nonce'], __CLASS__ ) ){
			$data['msg'] = __( 'Unable to delete this parallax!', DEX_MLP_TEXTDOMAIN );
			$deleted     = wp_delete_post( absint( $_POST['dex_mlp_id'] ), 'dex-mlp-parallax' );
			if( $deleted ){
				$data['done'] = true;
				$data['msg']  = __( 'Parallax deleted successfully!', DEX_MLP_TEXTDOMAIN );
			}
		}
		wp_send_json( $data );
	}

	public function ajax_create_parallax(){
		$data    = array(
			'done' => false,
			'msg'  => __( 'Unknown error', DEX_MLP_TEXTDOMAIN ),
		);
		if( isset( $_POST['dex_mlp_nonce'] ) && wp_verify_nonce( $_POST['dex_mlp_nonce'], __CLASS__ ) ){
			$mlpA = isset( $_POST['dex_mlp_parallax'] ) && is_array( $_POST['dex_mlp_parallax'] )
				? $_POST['dex_mlp_parallax'] : false;
			if( $mlpA ){
				$mlp = array();
				for ($i = 0, $c = count( $mlpA ); $i < $c; $i++){
					if( isset( $mlpA[ $i ]['name'] ) ){
						$mlp[ substr( $mlpA[ $i ]['name'], 0, -3 ) ] = $mlpA[ $i ]['value'];
					}
				}
				$name = isset( $mlp['name'] ) ? trim( $mlp['name'] ) : false;
				if( $name ){
					$mlp     = new DEX_MLP_Instance( $mlp );
					$post_id = wp_insert_post( array(
						'post_status'  => 'publish',
						'post_type'    => 'dex-mlp-parallax',
						'post_title'   => $name,
						'post_content' => addslashes( (string)$mlp ),
					) );
					if( $post_id ){
						$instances    = isset( $_POST['dex_mlp_count'] ) ? absint( $_POST['dex_mlp_count'] ) : 0;
						$data['done'] = true;
						$data['msg']  = __( 'Parallax created successfully!', DEX_MLP_TEXTDOMAIN );
						$data['obj']  = sprintf(
							$this->get_template( 'parallax-instance', 'no-white-space' ),
							( $instances + 1 ), // 1$
							esc_attr( (string)$mlp ), // 2$
							esc_html( $mlp->name ), // 3$
							__( 'Edit Parallax', DEX_MLP_TEXTDOMAIN ), // 4$
							__( 'Edit', DEX_MLP_TEXTDOMAIN ), // 5$
							__( 'Delete Parallax', DEX_MLP_TEXTDOMAIN ), // 6$
							( $mlp->parallax == 'scroll' ? ' checked="checked"' : '' ), // 7$
							( $mlp->parallax == 'mouse' ? ' checked="checked"' : '' ), // 8$
							( $mlp->layout == 'fullscreen' ? ' checked="checked"' : '' ), // 9$
							( $mlp->layout == 'custom' ? ' checked="checked"' : '' ), // 10$
							$mlp->width, // 11$
							$mlp->height, // 12$
							( $mlp->force_fw ? ' checked="checked"' : '' ), // 13$
							$mlp->margin_top, // 14$
							$mlp->margin_bottom, // 15$
							$mlp->padding_top, // 16$
							$mlp->padding_bottom, // 17$
							esc_attr( DEX_MLP_ASSETS . 'img/loader.gif' ), // 18$
							$post_id, // 19$
							__( 'Parallax Name', DEX_MLP_TEXTDOMAIN ), // 20$
							__( 'Scroll Parallax', DEX_MLP_TEXTDOMAIN ), // 21$
							__( 'Mouse Parallax', DEX_MLP_TEXTDOMAIN ), // 22$
							__( 'Fullscreen', DEX_MLP_TEXTDOMAIN ), // 23$
							__( 'Custom', DEX_MLP_TEXTDOMAIN ), // 24$
							__( 'Force Fullwidth', DEX_MLP_TEXTDOMAIN ), // 25$
							__( 'Margin Top', DEX_MLP_TEXTDOMAIN ), // 26$
							__( 'Margin Bottom', DEX_MLP_TEXTDOMAIN ), // 27$
							__( 'Padding Top', DEX_MLP_TEXTDOMAIN ), // 28$
							__( 'Padding Bottom', DEX_MLP_TEXTDOMAIN ), // 29$
							__( 'Save Settings', DEX_MLP_TEXTDOMAIN ), // 30$
							__( 'Edit Layers', DEX_MLP_TEXTDOMAIN ), // 31$
							__( 'Use shortcode', DEX_MLP_TEXTDOMAIN ), // 32$
							__( 'or', DEX_MLP_TEXTDOMAIN ) // 33$
						);
					} else {
						$data['msg'] = __( 'Failed to create your new parallax!', DEX_MLP_TEXTDOMAIN );
					}
				} else {
					$data['msg'] = __( 'Please name your parallax!', DEX_MLP_TEXTDOMAIN );
				}
			} else {
				$data['msg'] = __( 'Unable to create this parallax!', DEX_MLP_TEXTDOMAIN );
			}
		}
		wp_send_json( $data );
	}

	public function ajax_update_parallax(){
		$data    = array(
			'done' => false,
			'msg'  => __( 'Unknown error', DEX_MLP_TEXTDOMAIN ),
		);
		if( isset( $_POST['dex_mlp_nonce'], $_POST['dex_mlp_id'], $_POST['dex_mlp_instance'] ) && wp_verify_nonce( $_POST['dex_mlp_nonce'], __CLASS__ ) ){
			$mlpA = isset( $_POST['dex_mlp_parallax'] ) && is_array( $_POST['dex_mlp_parallax'] )
				? $_POST['dex_mlp_parallax'] : false;
			if( $mlpA ){
				$mlp = array();
				for ($i = 0, $c = count( $mlpA ); $i < $c; $i++){
					if( isset( $mlpA[ $i ]['name'] ) ){
						$k = str_replace(
							sprintf( '[%u]', absint( $_POST['dex_mlp_instance'] ) ),
							'', $mlpA[ $i ]['name']
						);
						$mlp[ $k ] = $mlpA[ $i ]['value'];
					}
				}
				$name = isset( $mlp['name'] ) ? trim( $mlp['name'] ) : false;
				if( $name ){
					$post_id = absint( $_POST['dex_mlp_id'] );
					$post    = get_post( $post_id );
					if( isset( $post->ID ) && $post->ID ){
						$dex = @json_decode( $post->post_content, true );
						if( $dex ){
							// since force fullwidth is a checkbox it`s not set in $_POST so we need to
							// manually set it otherwise once turned on it can`t be turned off because
							// it defaults to the ON state of $dex['force_fw']
							$mlp['force_fw'] = isset( $mlp['force_fw'] ) ? $mlp['force_fw'] : false;
							$mlp     = new DEX_MLP_Instance( wp_parse_args( $mlp, $dex ) );
							$JSON    = (string)$mlp;
							$updated = wp_update_post( array(
								'ID'           => $post_id,
								'post_title'   => $name,
								'post_content' => addslashes( $JSON ),
							), true );
							if( $updated ){
								if( is_wp_error( $updated ) ){
									$data['msg'] = $updated->get_error_message();
								} else {
									$data['done'] = true;
									$data['msg']  = __( 'Parallax updated successfully!', DEX_MLP_TEXTDOMAIN );
									$data['obj']  = $JSON;
								}
							} else {
								$data['msg'] = __( 'Failed to update your parallax!', DEX_MLP_TEXTDOMAIN );
							}
						} else {
							$data['msg'] = __( 'It seems that the requested parallax is corrupt!', DEX_MLP_TEXTDOMAIN );
						}
					} else {
						$data['msg'] = __( 'It seems that the requested parallax doesn`t exists!', DEX_MLP_TEXTDOMAIN );
					}
				} else {
					$data['msg'] = __( 'Please name your parallax!', DEX_MLP_TEXTDOMAIN );
				}
			} else {
				$data['msg'] = __( 'Unable to update this parallax!', DEX_MLP_TEXTDOMAIN );
			}
		}
		wp_send_json( $data );
	}

	public function ajax_load_layers(){
		$data    = array(
			'done' => false,
			'msg'  => __( 'Unknown error', DEX_MLP_TEXTDOMAIN ),
		);
		if( isset( $_POST['dex_mlp_nonce'], $_POST['dex_mlp_id'] ) && wp_verify_nonce( $_POST['dex_mlp_nonce'], __CLASS__ ) ){
			$post_id = absint( $_POST['dex_mlp_id'] );
			$post    = get_post( $post_id );
			if( isset( $post->ID ) && $post->ID ){
				$dex = @json_decode( $post->post_content, true );
				if( $dex ){
					$mlp      = new DEX_MLP_Instance( $dex );
					$overlay  = wp_get_attachment_image_src( $mlp->overlay_img, 'full' );
					$bg_image = wp_get_attachment_image_src( $mlp->bg_image, 'full' );
					$bg_color = $mlp->bg_color;
					switch( $mlp->background ){
						case 'bg_transparent':
							$img = $hex = '';
							break;
						case 'bg_image':
							$img = $bg_image[0];
							$hex = '';
							break;
						case 'bg_color':
							$img = '';
							$hex = $bg_color;
							break;
						case 'bg_video':
							$img = DEX_MLP_ASSETS . 'img/youtube-logo.jpg';
							$hex = '';
							break;
						default:
							$img = $hex = '';
					}
					$layers = $layer_list = '';
					if( $mlp->layers && is_array( $mlp->layers ) ){
						for( $i = 0, $c = count( $mlp->layers ); $i < $c; $i++ ){
							$inst   = $i + 1;
							$layer  = $mlp->layers[ $i ];
							// layer
							$css    = array();
							if( $layer->width > 0 ){
								$css[] = sprintf( 'width:%upx', $layer->width );
							}
							if( $layer->height > 0 ){
								$css[] = sprintf( 'height:%upx', $layer->height );
							}
							$css[]  = sprintf( 'left:%dpx', $layer->x );
							$css[]  = sprintf( 'top:%dpx', $layer->y );
							$css[]  = sprintf( 'opacity:%.1f', $layer->opacity );
							$css[]  = sprintf( 'z-index:%u', $inst );
							$layers.= sprintf(
								$this->get_template( 'parallax-layer', 'no-white-space' ),
								$inst, // 1$
								implode( ';', $css ), // 2$
								$layer->type == 'text' ?
									__( 'Text layer ', DEX_MLP_TEXTDOMAIN ) :
									__( 'Layer ', DEX_MLP_TEXTDOMAIN ), // 3$
								$layer->rotation, // 4$
								$layer->html // 5$
							);
							// layer list item
							$layer_list.= sprintf(
								$this->get_template( 'parallax-list-layer', 'no-white-space' ),
								$inst, // 1$
								$layer->type, // 2$
								$layer->width, // 3$
								$layer->height, // 4$
								$layer->rotation, // 5$
								__( 'hide this layer', DEX_MLP_TEXTDOMAIN ), // 6$
								__( 'lock this layer', DEX_MLP_TEXTDOMAIN ), // 7$
								$layer->type == 'text' ?
									__( 'Text Layer ', DEX_MLP_TEXTDOMAIN ) :
									__( 'Layer ', DEX_MLP_TEXTDOMAIN ), // 8$
								__( 'Speed: ', DEX_MLP_TEXTDOMAIN ), // 9$
								$layer->speed, // 10$
								__( 'Opacity: ', DEX_MLP_TEXTDOMAIN ), // 11$
								$layer->opacity, // 12$
								$layer->type != 'text' ?
									' style="display:none"' : '', // 13$
								$layer->html, // 14$
								__( 'Speed - Ratio', DEX_MLP_TEXTDOMAIN ), // 15$
								__( 'Opacity', DEX_MLP_TEXTDOMAIN ), // 16$
								__( 'Layer Position', DEX_MLP_TEXTDOMAIN ), // 17$
								$layer->x, // 18$
								$layer->y, // 19$
								__( 'Delete Layer', DEX_MLP_TEXTDOMAIN ) // 20$
							);
						}
					}
					$data['done'] = true;
					$data['msg']  = '';
					$data['obj']  = sprintf(
						$this->get_template( 'parallax-editor', 'no-white-space' ),
						$mlp->width, // 1$
						$mlp->height, // 2$
						( $mlp->height + 400 ) /* 200top + 200bottom */, // 3$
						esc_attr( $img ), // 4$
						esc_attr( $hex ), // 5$
						esc_attr( ( $mlp->overlay ? $overlay[0] : '' ) ), // 6$
						$layers, // 7$
						$layer_list, // 8$
						( $mlp->background == 'bg_image' ? ' checked="checked"' : '' ), // 9$
						( $mlp->background == 'bg_transparent' ? ' checked="checked"' : '' ), // 10$
						( $mlp->background == 'bg_color' ? ' checked="checked"' : '' ), // 11$
						( $mlp->background == 'bg_video' ? ' checked="checked"' : '' ), // 12$
						( $mlp->overlay ? ' checked="checked"' : '' ), // 13$
						(!$mlp->overlay ? ' checked="checked"' : '' ), // 14$
						$mlp->bg_image, // 15$
						$mlp->bg_color, // 16$
						$mlp->bg_video, // 17$
						$mlp->bg_speed, // 18$
						$mlp->overlay_img, // 19$
						esc_attr( $bg_image[0] ), // 20$
						esc_attr( $overlay[0] ), // 21$
						esc_attr( $post_id ), // 22$
						esc_attr( DEX_MLP_ASSETS . 'img/loader.gif' ), // 23$
						__( 'Create your parallax', DEX_MLP_TEXTDOMAIN ), // 24$
						__( 'Edit Layers', DEX_MLP_TEXTDOMAIN ), // 25$
						__( 'Background', DEX_MLP_TEXTDOMAIN ), // 26$
						__( 'Image', DEX_MLP_TEXTDOMAIN ), // 27$
						__( 'Browse', DEX_MLP_TEXTDOMAIN ), // 28$
						__( 'Transparent', DEX_MLP_TEXTDOMAIN ), // 29$
						__( 'Solid Color', DEX_MLP_TEXTDOMAIN ), // 30$
						__( 'Video', DEX_MLP_TEXTDOMAIN ), // 31$
						__( 'youtube video ID', DEX_MLP_TEXTDOMAIN ), // 32$
						__( 'Background speed', DEX_MLP_TEXTDOMAIN ), // 33$
						__( 'Add Layer', DEX_MLP_TEXTDOMAIN ), // 34$
						__( 'Text Layer', DEX_MLP_TEXTDOMAIN ), // 35$
						__( 'Image Layer', DEX_MLP_TEXTDOMAIN ), // 36$
						__( 'Overlay', DEX_MLP_TEXTDOMAIN ), // 37$
						__( 'On', DEX_MLP_TEXTDOMAIN ), // 38$
						__( 'Off', DEX_MLP_TEXTDOMAIN ), // 39$
						__( 'Delete All Layers', DEX_MLP_TEXTDOMAIN ), // 40$
						__( 'Save Settings', DEX_MLP_TEXTDOMAIN ), // 41$
						!$mlp->layers ? ' style="display:none"' : '' // 42$
					);
				} else {
					$data['msg'] = __( 'It seems that the requested parallax is corrupt!', DEX_MLP_TEXTDOMAIN );
				}
			} else {
				$data['msg'] = __( 'It seems that the requested parallax doesn`t exists!', DEX_MLP_TEXTDOMAIN );
			}
		}
		wp_send_json( $data );
	}

	public function ajax_update_layers(){
		$data    = array(
			'done' => false,
			'msg'  => __( 'Unknown error', DEX_MLP_TEXTDOMAIN ),
		);
		if( isset( $_POST['dex_mlp_nonce'], $_POST['dex_mlp_id'], $_POST['dex_mlp_layers'] ) && wp_verify_nonce( $_POST['dex_mlp_nonce'], __CLASS__ ) ){
			$post_id = absint( $_POST['dex_mlp_id'] );
			$post    = get_post( $post_id );
			if( isset( $post->ID ) && $post->ID ){
				$dex = @json_decode( $post->post_content, true );
				if( $dex ){
					$mlpA = isset( $_POST['dex_mlp_layers'] ) && is_array( $_POST['dex_mlp_layers'] )
						? $_POST['dex_mlp_layers'] : false;
					if( $mlpA ){
						$mlp = array( 'layers' => array() );
						for ($i = 0, $c = count( $mlpA ); $i < $c; $i++){
							if( isset( $mlpA[ $i ]['name'] ) ){
								if( strpos( $mlpA[ $i ]['name'], '[]' ) !== false ){ // is array
									$mlp['layers'][ substr( $mlpA[ $i ]['name'], 0, -2 ) ][] = $mlpA[ $i ]['value'];
								} else {
									$mlp[ $mlpA[ $i ]['name'] ] = $mlpA[ $i ]['value'];
								}
							}
						}
						if( $mlp['layers'] && is_array( $mlp['layers'] ) ){
							$layers = array();
							for( $i = 0, $c = count( $mlp['layers']['html'] ); $i < $c; $i++ ){
								$layers[] = array(
									'width'    => isset( $mlp['layers']['width'][ $i ] ) ?
										$mlp['layers']['width'][ $i ] : null,
									'height'   => isset( $mlp['layers']['height'][ $i ] ) ?
										$mlp['layers']['height'][ $i ] : null,
									'rotation' => isset( $mlp['layers']['rotation'][ $i ] ) ?
										$mlp['layers']['rotation'][ $i ] : null,
									'type'     => isset( $mlp['layers']['type'][ $i ] ) ?
										$mlp['layers']['type'][ $i ] : null,
									'html'     => isset( $mlp['layers']['html'][ $i ] ) ?
										stripslashes( $mlp['layers']['html'][ $i ] ) : null,
									'speed'    => isset( $mlp['layers']['speed'][ $i ] ) ?
										$mlp['layers']['speed'][ $i ] : null,
									'opacity'  => isset( $mlp['layers']['opacity'][ $i ] ) ?
										$mlp['layers']['opacity'][ $i ] : null,
									'x'        => isset( $mlp['layers']['x'][ $i ] ) ?
										$mlp['layers']['x'][ $i ] : null,
									'y'        => isset( $mlp['layers']['y'][ $i ] ) ?
										$mlp['layers']['y'][ $i ] : null,
								);
							}
							$mlp['layers'] = $layers;
						}
						$mlp     = new DEX_MLP_Instance( wp_parse_args( $mlp, $dex ) );
						$JSON    = addslashes( (string)$mlp );
						$data['json'] = (string)$mlp;
						$updated = wp_update_post( array(
							'ID'           => $post_id,
							'post_content' => $JSON,
						), true );
						if( $updated ){
							if( is_wp_error( $updated ) ){
								$data['msg'] = $updated->get_error_message();
							} else {
								$data['done'] = true;
								$data['msg']  = __( 'Parallax updated successfully!', DEX_MLP_TEXTDOMAIN );
							}
						} else {
							$data['msg'] = __( 'Failed to update your parallax!', DEX_MLP_TEXTDOMAIN );
						}
					} else {
						$data['msg'] = __( 'Unable to update this parallax!', DEX_MLP_TEXTDOMAIN );
					}
				} else {
					$data['msg'] = __( 'It seems that the requested parallax is corrupt!', DEX_MLP_TEXTDOMAIN );
				}
			} else {
				$data['msg'] = __( 'It seems that the requested parallax doesn`t exists!', DEX_MLP_TEXTDOMAIN );
			}
		}
		wp_send_json( $data );
	}

}
