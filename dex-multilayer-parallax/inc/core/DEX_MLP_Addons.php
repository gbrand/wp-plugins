<?php

defined( 'ABSPATH' ) or die();

/***************************************
* DEX MULTILAYER PARALLAX ADDONS CLASS
***************************************/
class DEX_MLP_Addons {

	private $mlps = array();

	public function __construct(){
		add_action( 'admin_enqueue_scripts', array( $this, 'mce_register' ) );
		add_action( 'vc_before_init', array( $this, 'vc_register' ) );
	}

	public function mce_register(){
		if( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ){
			return;
		}
		global $typenow;
		if( get_user_option( 'rich_editing' ) == 'true' ){
			if( empty( $typenow ) && !empty( $_GET['post'] ) ){
				$post = get_post( $_GET['post'] );
				$typenow = $post->post_type;
			}
			$post_type = isset( $_GET['post_type'] ) ? $_GET['post_type'] : false;
			if( $typenow !== 'dex-mlp-parallax' && $post_type !== 'dex-mlp-parallax' ){
				add_filter( 'mce_buttons', array( $this, 'mce_buttons' ) );
				add_filter( 'mce_external_plugins', array( $this, 'mce_external_plugins' ) );
				$this->mce_localize();
			}
		}
	}

	public function mce_buttons( $buttons ){
		array_push( $buttons, 'dex_mlp_parallax' );
		return $buttons;
	}

	public function mce_external_plugins( $plugin_array ){
		$plugin_array['dex_mlp_parallax'] = DEX_MLP_ASSETS . 'js/dex-mlp-tinymce.js';
		return $plugin_array;
	}

	public function mce_localize(){
		$mlps = array();
		foreach( $this->get_dex_mlps() as $mlp ){
			$mlps[] = array(
				'text'  => $mlp['name'],
				'value' => $mlp['id'],
			);
		}
		wp_localize_script( 'jquery', __CLASS__, array(
			'i18n' => array(
				'mce_text'   => __( 'DEX::MLP', DEX_MLP_TEXTDOMAIN ),
				'mce_title'  => __( 'DEX::MLP', DEX_MLP_TEXTDOMAIN ),
				'mce_select' => __( 'Select a parallax', DEX_MLP_TEXTDOMAIN ),
				'mce_empty'  => __( 'You haven`t created any parallax, yet.', DEX_MLP_TEXTDOMAIN ),
			),
			'icon' => DEX_MLP_ASSETS . 'img/dex-icon-transparent.png',
			'mlps' => array(
				'count'  => count( $mlps ),
				'values' => $mlps,
			),
		) );
	}

	public function vc_register(){
		if( function_exists( 'vc_map' ) ){
			$values = array(
				__( 'Select multilayer parallax', DEX_MLP_TEXTDOMAIN ) => 0,
			);
			if( count( $this->get_dex_mlps() ) > 0 ){
				$i = 1;
				foreach( $this->get_dex_mlps() as $mlp ){
					$values[ $i . '. ' . $mlp['name'] ] = $mlp['id'];
					$i++;
				}
			}
			$params = array(
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Parallax', DEX_MLP_TEXTDOMAIN ),
					'param_name'  => 'id',
					'value'       => $values,
					'std'         => 0,
					'description' => __( "Select one of your prebuild parallax`s", DEX_MLP_TEXTDOMAIN ),
				),
			);
			vc_map( array(
				'name'                    => __( 'DEX::MLP', DEX_MLP_TEXTDOMAIN ),
				'base'                    => 'dex_mlp_parallax',
				'as_parent'               => array(
					'except' => 'dex_mlp_parallax'
				),
				'content_element'         => true,
				'show_settings_on_create' => true,
				'params'                  => $params,
				'description'             => __( 'Multilayer Parallax', DEX_MLP_TEXTDOMAIN ),
				'icon'                    => DEX_MLP_ASSETS . 'img/dex-icon-transparent.png',
				'js_view'                 => 'VcColumnView',
			) );
			WPBakeryShortCode_cls();
		}
	}

	private function get_dex_mlps(){
		if( !$this->mlps ){
			$mlps = get_posts( array(
				'post_status'    => 'publish',
				'post_type'      => 'dex-mlp-parallax',
				'posts_per_page' => -1,
				'orderby'        => 'name',
				'order'          => 'ASC',
			) );
			if( $mlps ){
				foreach( $mlps as $mlp ){
					$d = @json_decode( $mlp->post_content, true );
					if( $d ){
						$this->mlps[ $mlp->ID ] = array(
							'id'       => $mlp->ID,
							'name'     => $mlp->post_title,
							'settings' => $d,
						);
					}
				}
			}
		}
		return $this->mlps;
	}


}

/***************************************
* DEX MULTILAYER PARALLAX VC SCC CLASS
***************************************/
function WPBakeryShortCode_cls(){
	if( class_exists( 'WPBakeryShortCodesContainer' ) ){
		class WPBakeryShortCode_DEX_MLP_Parallax extends WPBakeryShortCodesContainer {}
	}
}
