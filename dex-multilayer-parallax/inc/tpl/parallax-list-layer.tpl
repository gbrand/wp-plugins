<li id="dex-list-layer-%1$u" data-layer="#dex-layer-%1$u">
	<input type="hidden" name="type[]" class="dex-layer-type" value="%2$s"/>
	<input type="hidden" name="width[]" class="dex-layer-width" value="%3$u"/>
	<input type="hidden" name="height[]" class="dex-layer-height" value="%4$u"/>
	<input type="hidden" name="rotation[]" class="dex-layer-rotation" value="%5$f"/>
	<div class="dex-mlp-layers">
		<a class="dex-mlp-layers-visibility" href="#" title="%6$s"><i class="dex-mlp-icon-visible"></i></a>
		<a class="dex-mlp-layers-lock" href="#" title="%7$s"><i class="dex-mlp-icon-unlock"></i></a>
		<span class="dex-mlp-layers-name">%8$s %1$u <span class="dex-mlp-config">( %9$s %10$.2f, %11$s %12$.1f )</span></span>
	</div>
	<div class="dex-mlp-layers-settings">
		<div class="dex-mlp-row" %13$s >
			<div class="dex-mlp-col12">
				<textarea name="html[]" class="dex-mlp-html" col="10" rows="3">%14$s</textarea>
			</div>
		</div>
		<div class="dex-mlp-row">
			<div class="dex-mlp-col6">
				<p>%15$s</p>
				<p class="text-center clearfix">
					<label class="float-left">0.5</label>
					<label>1.25</label>
					<label class="float-right">2</label>
				</p>
				<input type="range" class="dex-layer-speed" name="speed[]" min="0.5" max="2" step="0.05" value="%10$.2f"/>
			</div>
			<div class="dex-mlp-col6">
				<p>%16$s</p>
				<p class="clearfix">
					<label class="float-left">0</label>
					<label class="float-right">1</label>
				</p>
				<input type="range" class="dex-layer-opacity" name="opacity[]" min="0" max="1" step="0.1" value="%12$.1f"/>
			</div>
		</div>
		<div class="dex-mlp-row">
			<div class="dex-mlp-col8">
				<p>
					%17$s
					<label>X:</label>
					<input type="text" class="dex-layer-pos-x" name="x[]" value="%18$d"/>
					<label>Y:</label>
					<input type="text" class="dex-layer-pos-y" name="y[]" value="%19$d"/>
					<label>px</label>
				</p>
			</div>
			<div class="dex-mlp-col4">
				<p class="text-right"><a class="btn" href="#" id="dex-mlp-delete-layer">%20$s</a></p>
			</div>
		</div>
	</div>
</li>
