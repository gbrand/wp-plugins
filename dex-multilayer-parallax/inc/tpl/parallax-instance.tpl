<li data-config="%2$s">
	<form id="dex-mlp-form-%1$u" onsubmit="return false;">
	<div class="dex-mlp-parallax">
		<span class="dex-mlp-name">%3$s</span> <span class="dex-mlp-config"></span>
		<span class="dex-mlp-parallax-list-edit">
			<a href="#" title="%4$s" data-id="%19$u">%5$s</a>
		</span>
		<span class="dex-mlp-parallax-list-delete">
			<a href="#" title="%6$s" data-id="%19$u"><i class="dex-mlp-icon-trash"></i></a>
		</span>
	</div>
	<div class="dex-mlp-parallax-settings">
		<div class="dex-mlp-row">
			<div class="dex-mlp-col12">
				<p>%32$s
					<span class="dex-mlp-shortcode">[dex_mlp_parallax id="%19$u"]</span> %33$s
					<span class="dex-mlp-shortcode">[dex_mlp_parallax id="%19$u"][/dex_mlp_parallax]</span>
				</p>
			</div>
		</div>
		<hr class="dex-mlp-hr"/>
		<div class="dex-mlp-row">
			<div class="dex-mlp-col4">
				<input type="text" name="name[%1$u]" value="%3$s" class="dex-mlp-parallax-name" placeholder="%20$s" />
			</div>
			<div class="dex-mlp-col1"></div>
			<div class="dex-mlp-col7">
				<div class="dex-mlp-parallax-type">
					<label>
						<input type="radio" name="parallax[%1$u]" value="scroll" %7$s />
						%21$s
					</label>
					<label>
						<input type="radio" name="parallax[%1$u]" value="mouse" %8$s />
						%22$s
					</label>
				</div>
				<div class="dex-mlp-parallax-size">
					<label>
						<input type="radio" class="dex-mlp-lyt-fs" name="layout[%1$u]" value="fullscreen" %9$s />
						%23$s
					</label>
					<label>
						<input type="radio" class="dex-mlp-lyt-cs" name="layout[%1$u]" value="custom"  %10$s />
						%24$s
					</label>
					<input type="text" class="dex-mlp-width" name="width[%1$u]" placeholder="960" value="%11$u" />
					<label>x</label>
					<input type="text" class="dex-mlp-height" name="height[%1$u]" placeholder="480" value="%12$u" />
					<label>px</label>
					<label>
						<input type="checkbox" name="force_fw[%1$u]" value="force_fw" %13$s />
						%25$s
					</label>
				</div>
			</div>
		</div>
		<div class="dex-mlp-parallax-position">
			<div class="dex-mlp-row">
				<div class="dex-mlp-col3">
					<label>%26$s</label>
					<input type="text" name="margin_top[%1$u]" value="%14$u" placeholder="0">
					<label>px</label>
				</div>
				<div class="dex-mlp-col3">
					<label>%27$s</label>
					<input type="text" name="margin_bottom[%1$u]" value="%15$u" placeholder="0">
					<label>px</label>
				</div>
				<div class="dex-mlp-col3">
					<label>%28$s</label>
					<input type="text" name="padding_top[%1$u]" value="%16$u" placeholder="0">
					<label>px</label>
				</div>
				<div class="dex-mlp-col3">
					<label>%29$s</label>
					<input type="text" name="padding_bottom[%1$u]" value="%17$u" placeholder="0">
					<label>px</label>
				</div>
			</div>
		</div>
		<div class="dex-mlp-row">
			<div class="dex-mlp-col12">
				<p class="text-center">
					<button type="button" class="btn dex-mlp-save-parallax" data-instance="%1$u" data-id="%19$u">%30$s</button>
					<button type="button" class="btn dex-mlp-edit-layers" data-id="%19$u">%31$s</button>
				</p>
				<p class="text-center" style="display:none;">
					<img src="%18$s" alt=""/>
				</p>
			</div>
		</div>
	</div>
	</form>
</li>
