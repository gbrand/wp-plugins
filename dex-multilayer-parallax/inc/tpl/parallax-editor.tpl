<div id="dex-mlp-preview" style="height:%3$upx;">
	<!--<div id="dex-mlp-preview-content">
		<i class="dex-mlp-icon-inbox"></i>
		<h3><strong>%24$s</strong></h3>
	</div>-->
	<div id="dex-mlp-layers-container" class="dex-mlp-layers-container" style="width:%1$upx; height:%2$upx;background-image:url('%4$s');background-color:%5$s;">
		<div class="dex-mlp-overlay" style="background-image:url('%6$s');"></div>
		<div id="dex-mlp-preview-layers">%7$s</div>
		<div id="dex-mlp-axis-x"></div>
		<div id="dex-mlp-axis-y"></div>
	</div>
</div>
<div id="dex-mlp-layers">
	<form id="dex-editor-form" onsubmit="return false;">
		<h2 class="dex-mlp-section-title">%25$s</h2>
		<div class="dex-mlp-row">
			<div class="dex-mlp-col4">
				<div class="dex-mlp-bg-options">
					<a href="#">%26$s</a>
					<ul>
						<li>
							<label for="dex-mlp-bg-image">
								<input type="radio" name="background" id="dex-mlp-bg-image" class="dex-mlp-bg-change" value="bg_image" %9$s />
								%27$s
							</label>
							<label id="dex-mlp-browse">
								%28$s
								<input type="hidden" name="bg_image" id="dex-mlp-image" value="%15$u" data-url="%20$s"/>
							</label>
						</li>
						<li>
							<label for="dex-mlp-bg-transparent">
								<input type="radio" name="background" id="dex-mlp-bg-transparent" class="dex-mlp-bg-change" value="bg_transparent" %10$s />
								%29$s
							</label>
						</li>
						<li>
							<label for="dex-mlp-bg-solid-color">
								<input type="radio" name="background" id="dex-mlp-bg-solid-color" class="dex-mlp-bg-change" value="bg_color" %11$s />
								%30$s
							</label>
							<input type="text" name="bg_color" id="dex-mlp-bg-color" value="%16$s" data-default-color="%16$s"/>
						</li>
						<li>
							<label for="dex-mlp-bg-video">
								<input type="radio" name="background" id="dex-mlp-bg-video" class="dex-mlp-bg-change" value="bg_video" %12$s />
								%31$s
							</label>
							<input type="text" name="bg_video" id="dex-mlp-youtube-link" placeholder="%32$s" value="%17$s" maxlength="11"/>
						</li>
						<li class="bg-speed-settings">
							<p>%33$s</p>
							<label class="float-left">0</label>
							<label class="float-right">1</label>
							<input type="range" name="bg_speed" id="bg-speed-settings" min="0" max="1" step="0.1" value="%18$.1f" />
						</li>
					</ul>
				</div>
				<div class="dex-mlp-add-layers">
					<a href="#">%34$s</a>
					<ul>
						<li>
							<a href="#" id="dex-mlp-new-text">%35$s</a>
						</li>
						<li>
							<a href="#" id="dex-mlp-new-image">%36$s</a>
						</li>
					</ul>
				</div>
				<div class="dex-mlp-pattern">
					<p>
						%37$s
						<label for="dex-mlp-overlay-on">
							<input type="radio" name="overlay" id="dex-mlp-overlay-on" class="dex-mlp-overlay-change" value="On" %13$s />
							%38$s
						</label>
						<label for="dex-mlp-overlay-off">
							<input type="radio" name="overlay" id="dex-mlp-overlay-off" class="dex-mlp-overlay-change" value="" %14$s />
							%39$s
						</label>
					</p>
					<label id="dex-mlp-browse-pattern">
						%28$s
						<input type="hidden" name="overlay_img" id="dex-mlp-pattern" value="%19$u" data-url="%21$s"/>
					</label>
				</div>
			</div>
			<div class="dex-mlp-col8">
				<ol class="dex-mlp-layers-list">%8$s</ol>
				<p><button id="dex-mlp-rm-all-layers" class="btn" type="button" %42$s >%40$s</button></p>
			</div>
		</div>
		<div class="dex-mlp-row">
			<div class="dex-mlp-col12">
				<p class="text-center">
					<button type="button" id="dex-mlp-save-layers" class="btn" data-id="%22$u">%41$s</button>
					<img src="%23$s" alt="" style="display:none;" />
				<p>
			</div>
		</div>
	</form>
</div>
