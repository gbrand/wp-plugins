<?php
/*
Plugin Name: DEX Multilayer Parallax
Plugin URI:  
Description: Dex Multilayer Parallax Wp is a wordpress plugin that allows you to create beautiful parallax sections with more that one layer. Be creative, combine layers in any style you want. We create 12 different examples to guide your first steps to a beautiful multilayer world. Play with layers.
Version:     1.1.0
Domain Path: /languages
Text Domain: dex-multilayer-parallax
*/

defined( 'ABSPATH' ) or die();

define( 'DEX_MLP_VERSION', '1.1.0' );
define( 'DEX_MLP_ROOT', plugin_dir_path( __FILE__ ) );
define( 'DEX_MLP_INC', DEX_MLP_ROOT . 'inc' . DIRECTORY_SEPARATOR );
define( 'DEX_MLP_CORE', DEX_MLP_INC . 'core' . DIRECTORY_SEPARATOR );
define( 'DEX_MLP_TPL', DEX_MLP_INC . 'tpl' . DIRECTORY_SEPARATOR );
define( 'DEX_MLP_TEXTDOMAIN', 'dex-multilayer-parallax' );
define( 'DEX_MLP_ASSETS', plugins_url( 'assets/', __FILE__ ) );
define( 'DEX_MLP_BASENAME', dirname( plugin_basename( __FILE__ ) ) );

require_once DEX_MLP_CORE . 'DEX_MLP_Addons.php';
require_once DEX_MLP_CORE . 'DEX_MLP_Backbone.php';
require_once DEX_MLP_CORE . 'DEX_MLP_Manager.php';
require_once DEX_MLP_CORE . 'DEX_MLP_Setup.php';

register_activation_hook( __FILE__, array( 'DEX_MLP_Setup', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'DEX_MLP_Setup', 'deactivate' ) );

DEX_MLP_Manager::run();
